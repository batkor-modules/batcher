<?php


namespace Drupal\batcher_example\Service;


class BatcherExample {
  public static function process($item, $my_var, &$context) {
    vd($item);
    vd($my_var);
  }
  public static function finish($success, $results, $operations) {
    \Drupal::messenger()->addMessage(__CLASS__ . ':'. __FUNCTION__);
  }
}
