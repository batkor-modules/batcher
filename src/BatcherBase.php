<?php

namespace Drupal\batcher;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Queue\BatchMemory;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

abstract class BatcherBase extends PluginBase implements BatcherInterface {

  /**
   * {@inheritdoc}
   */
  public static function create($plugin_id, $items = [], $extra_data = []) {
    /** @var \Drupal\batcher\BatcherPluginManager $batcher_manager */
    $batcher_manager = \Drupal::service('plugin.manager.batcher');
    $data = [
        'items' => $items,
      ] + $extra_data;

    return $batcher_manager->createInstance($plugin_id, ['data' => $data]);
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    $builder = $this->buildBatch();
    batch_set($builder->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function runInBackend() {
    $this->run();
    if (function_exists('drush_backend_batch_process')) {
      try {
        // @see https://github.com/drush-ops/drush/issues/3773
        $result = drush_backend_batch_process();
        if ($result['context']['drush_batch_process_finished'] === TRUE) {
          $batch = &batch_get();
          $batch = NULL;
        }
      } catch (\InvalidArgumentException $e) {
        \Drupal::logger('batcher')->error($e->getMessage());
        $this->noProgressiveBatchProcess();
      }
    }
    else {
      $this->noProgressiveBatchProcess();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return (string) $this->pluginDefinition['description'];
  }

  public function statusProcessMessage() {
    return $this->pluginDefinition['status_process'];
  }

  /**
   * Run no progressive batch process
   */
  protected function noProgressiveBatchProcess() {
    // See https://www.drupal.org/node/638712
    $batch = &batch_get();
    $batch['progressive'] = FALSE;
    batch_process();
  }

  /**
   * Returns elements per operation plugin limit.
   *
   * Default 50 elements.
   *
   * @return int
   */
  protected function limit() {
    return (int) $this->pluginDefinition['limit'];
  }

  /**
   * Returns data for process function from plugin configuration.
   *
   * @return array
   *   The data key in plugin configuration.
   */
  protected function data() {
    return $this->configuration['data'];
  }

  /**
   * Returns plugin process callback.
   *
   * @return string
   *   The process callback.
   */
  protected function getProcessCallback() {
    $process_callback = $this->pluginDefinition['callbacks']['process'];

    return $this->getCallbackFromDefinition($process_callback);
  }

  /**
   * Returns plugin process callback.
   *
   * @return string
   *   The process callback.
   */
  protected function getFinishCallback() {
    $finish_callback = $this->pluginDefinition['callbacks']['finish'];

    return $this->getCallbackFromDefinition($finish_callback);
  }

  /**
   * Parse and returns callback from plugin.
   *
   * @param $callback
   *
   * @return mixed
   */
  protected function getCallbackFromDefinition($callback) {

    if (function_exists($callback)) {
      return $callback;
    }

    $count = substr_count($callback, ':');

    if ($count == 1) {
      list($class_or_service, $method) = explode(':', $callback, 2);
    }
    elseif (strpos($callback, '::') !== FALSE) {
      list($class_or_service, $method) = explode('::', $callback, 2);
    }

    $class_or_service = \Drupal::service('class_resolver')
      ->getInstanceFromDefinition($class_or_service);
    $callback = [$class_or_service, $method];

    if (!is_callable($callback)) {
      throw new \LogicException(sprintf('Unable to parse the "callback" name "%s".', $callback));
    }

    return $callback;
  }

}
