<?php

namespace Drupal\batcher;

/**
 * Interface for batcher plugins.
 */
interface BatcherInterface {

  /**
   * Returns plugin instance.
   *
   * @param $plugin_id
   *   Plugin id.
   * @param $items
   *   Items for send in process callback.
   * @param array $extra_data
   *   Extra data for send in process callback.
   *
   * @return BatcherInterface|object
   */
  public static function create($plugin_id, $items, $extra_data);

  /**
   * Run batch.
   */
  public function run();

  /**
   * Run batch using drush backend batch process.
   *
   * @see \drush_backend_batch_process
   */
  public function runInBackend();

  /**
   * Build batch builder object for plugin.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   */
  public function buildBatch();

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the translated plugin description.
   *
   * @return string
   *   The translated description.
   */
  public function description();

}
