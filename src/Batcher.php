<?php

namespace Drupal\batcher;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Default batcher plugins
 */
class Batcher extends BatcherBase {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildBatch() {
    $builder = new BatchBuilder();
    $builder->setTitle($this->label());
    $builder->setProgressMessage($this->description());
    $args = [$this->data(), 'status_process' => $this->statusProcessMessage()];
    $builder->addOperation([$this, 'masterProcess'], $args);
    $builder->setFinishCallback([$this, 'masterFinish']);
    return $builder;
  }

  /**
   * Master process callback.
   *
   * @param $data
   * @param $context
   *
   * @see \Drupal\batcher\Batcher::buildBatch
   */
  public function masterProcess($data, $status_process, &$context) {
    $limit = $this->limit();

    // Set default progress values.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($data['items']);
      $context['sandbox']['items'] = $data['items'];
    }

    // Clear data for send in child callback.
    unset($data['items']);

    // Remove already processed items.
    if ($context['sandbox']['progress']) {
      array_splice($context['sandbox']['items'], 0, $limit);
    }

    foreach ($context['sandbox']['items'] as $item) {
      if (!($limit)) {
        break;
      }
      $limit--;

      // @todo Check optimal realization.
      //$args = [$item] + $data + [&$context];
      $args = [$item] + $data;
      $args[] = &$context;
      call_user_func_array($this->getProcessCallback(), $args);
      $context['sandbox']['progress']++;

      $context['message'] = t($status_process, [
        ':progress' => $context['sandbox']['progress'],
        ':total' => $context['sandbox']['max'],
      ]);
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Master finish callback.
   *
   * @param $success
   * @param $results
   * @param $operations
   *
   * @see \Drupal\batcher\Batcher::buildBatch
   */
  public function masterFinish($success, $results, $operations) {
    call_user_func_array($this->getFinishCallback(), [$success, $results, $operations]);
  }

}
