# Batcher

This module providers plugin for fast using batch Api.

### Problem/Motivation
Creating batch process very expensive. [batch operations](https://api.drupal.org/api/drupal/core%21includes%21form.inc/group/batch/8.7.x)

#### Example simple batch
```php
/**
 * Function for run batch.
 */
function run_my_batch($is_backend = FALSE) {
  $my_args = [];
  ...
  $builder = new BatchBuilder();
  $builder->addOperation('my_batch_process_callback', [$my_args]);
  $builder->setFinishCallback('my_batch_finish_callback');
  batch_set($builder->toArray());
  
  if ($is_backend) {
    drush_backend_batch_process();
  }
}

/**
 * Process function for batch.
 */
function my_batch_process_callback($data, &$context) {
  ...
} 

/**
 * Finish function for batch.
 */
function my_batch_finish_callback($success, $results, $operations) {
  ...
} 
```

#### A more advanced example
This example is taken from [this](https://niklan.net/blog/192).
```php
/**
 * Function for run batch.
 */
function run_my_batch($is_backend = FALSE) {
  $my_args = [];
  ...
  $builder = new BatchBuilder();
  $builder->addOperation('my_batch_master_process_callback', [$my_args]);
  $builder->setFinishCallback('my_batch_finish_callback');
  batch_set($builder->toArray());
  
  if ($is_backend) {
    drush_backend_batch_process();
  }
}

/**
 * Process function for batch.
 */
function my_batch_master_process_callback($items, &$context) {
  // Elements per operation.
  $limit = 50;
  
  // Set default progress values.
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($items);
  }

  // Save items to array which will be changed during processing.
  if (empty($context['sandbox']['items'])) {
    $context['sandbox']['items'] = $items;
  }

  $counter = 0;
  if (!empty($context['sandbox']['items'])) {
    // Remove already processed items.
    if ($context['sandbox']['progress'] != 0) {
      array_splice($context['sandbox']['items'], 0, $limit);
    }

    foreach ($context['sandbox']['items'] as $item) {
    
      if ($counter != $limit) {
        my_batch_process_callback($item, $context);

        $counter++;
        $context['sandbox']['progress']++;

        $context['message'] = $this->t('Now processing node :progress of :count', [
          ':progress' => $context['sandbox']['progress'],
          ':count' => $context['sandbox']['max'],
        ]);

        // Increment total processed item values. Will be used in finished
        // callback.
        $context['results']['processed'] = $context['sandbox']['progress'];
      }
    }
  }

  // If not finished all tasks, we count percentage of process. 1 = 100%.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
} 

/**
 * Process function for batch.
 */
function my_batch_process_callback($item, &$context) {
  ...
} 

/**
 * Finish function for batch.
 */
function my_batch_finish_callback($success, $results, $operations) {
  ...
} 
```
### Proposed resolution

As said before this module providers plugin for fast using.

#### Example using this plugin

1. Announce your plugin in `my_module.batcher.yml`. More example in child module `batcher_example`;
    ```yaml
    function_batch:
      label: "Callbacks functions"
      description: "Example callbacks functions"
      status_process: "Function now processing node :progress of :total"
      callbacks:
        process: first_batch_process
        finish: first_batch_finish
    ```
2. Create your callback functions/ObjectMethod/ServiceMethod;
    ```php
     function first_batch_process($item, $my_extra_data, &$context) {
       ...
     }
        
     function first_batch_finish($success, $results, $operations) {
       ...
     }
    ```
3. Run your batch.
    
    Simple run:
        
    ```php
      Batcher::create('function_batch', ['1', '2'], ['my_extra_data' => 'extra_data'])->run();
    ```
    Run in backend:
            
    ```php
      Batcher::create('function_batch', ['1', '2'], ['my_extra_data' => 'extra_data'])->runInBackend();
    ```
